<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator;

interface GeneratorManagerInterface
{
    public static function getGenerators(): array;
}
