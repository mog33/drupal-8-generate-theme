<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

use Mog33\Drupal8GenTheme\Generator\BaseGenerator;

/**
 * Generator for drupal/bootstrap.
 *
 * @link https://drupal-bootstrap.org/api/bootstrap/docs!Sub-Theming.md/group/sub_theming/8.x-3.x
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class Bootstrap extends BaseGenerator implements PluginGeneratorInterface
{
    /**
     * Settings for this theme generation.
     *
     * @var array
     */
    public static $settings = [
        'id' => 'bootstrap',
        'base-theme' => 'bootstrap',
        'name_placeholder' => 'THEMENAME',
        'title_placeholder' => 'THEMETITLE',
        'starterkit_path' => '/themes/contrib/bootstrap/starterkits/THEMENAME',
        'files' => [
            'THEMENAME.theme',
            'THEMENAME.starterkit.yml',
            'THEMENAME.libraries.yml',
            'config/install/THEMENAME.settings.yml',
            'config/schema/THEMENAME.schema.yml',
        ],
        'dependencies' => [
            'drupal/bootstrap' => '^3.21',
            'twbs/bootstrap' => '~3.4',
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function instruction(): void
    {
        // @codingStandardsIgnoreLine
        $this->IO->write('<info>This theme rely on Bootstrap 3, for Bootstrap 4 support please use bootstrap_barrio base theme.</info>');
        if (!$this->IO->askConfirmation('Continue? (y/n) [n] ', false)) {
            $this->IO->warning('SubTheme creation skipped.');
            exit;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function editYaml(): void
    {
        // The starterkit info.yml file is named starterkit.yml, so let's change it.
        $file = $this->config['custom_path'] . $this->config['name'] . '.starterkit.yml';
        $newFile = $this->config['custom_path'] . $this->config['name'] . '.info.yml';
        $this->fs->rename($file, $newFile, true);
        // Main process.
        parent::editYaml();
    }
}
