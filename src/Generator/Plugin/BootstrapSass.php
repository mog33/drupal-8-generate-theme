<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

use Mog33\Drupal8GenTheme\Generator\BaseGenerator;
use Mog33\Drupal8GenTheme\Helpers\Utils;
use Symfony\Component\Yaml\Yaml;

/**
 * Generator for drupal/bootstrap with Sass support.
 *
 * @link https://drupal-bootstrap.org/api/bootstrap/docs!Sub-Theming.md/group/sub_theming/8.x-3.x#sass
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class BootstrapSass extends Bootstrap implements PluginGeneratorInterface
{
    /**
     * Settings for this theme generation.
     *
     * @var array
     */
    public static $settings = [
        'id' => 'bootstrap_sass',
        'base-theme' => 'bootstrap',
        'name_placeholder' => 'THEMENAME',
        'title_placeholder' => 'THEMETITLE',
        'starterkit_path' => '/themes/contrib/bootstrap/starterkits/THEMENAME',
        'files' => [
            'THEMENAME.theme',
            'THEMENAME.starterkit.yml',
            'THEMENAME.libraries.yml',
            'config/install/THEMENAME.settings.yml',
            'config/schema/THEMENAME.schema.yml',
        ],
        'dependencies' => [
            'drupal/bootstrap' => '^3.21',
            'twbs/bootstrap' => '~3.4',
            'unicorn-fail/drupal-bootstrap-styles' => '^0.0.2',
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function finalize(): void
    {
        // Empty style.css.
        $this->fs->remove($this->config['custom_path'] . 'css/style.css');
        $this->fs->dumpFile($this->config['custom_path'] . 'css/style.css', '');

        // Switch starterkit libraries to use Bootstrap Sass javascript files.
        $file = $this->config['custom_path'] . $this->config['name'] . '.libraries.yml';

        // Keep css values and adapt js.
        $yaml = Yaml::parseFile($file);
        $add['framework'] = [
            'css' => $yaml['framework']['css'],
            'js' => [
                '/libraries/bootstrap/dist/js/bootstrap.bundle.min.js' => [],
            ],
        ];
        Utils::yamlEdit($this->config['custom_path'] . $this->config['name'] . '.libraries.yml', $add);

        // Copy Bootstrap Sass library.
        if ($this->fs->exists($this->config['drupal_doc_root'] . '/vendor/twbs/bootstrap-sass/assets')) {
            // Remove to replace if already exist.
            if ($this->fs->exists($this->config['custom_path'] . '/bootstrap/assets')) {
                $this->fs->remove($this->config['custom_path'] . '/bootstrap/assets');
            }
            $this->fs->mirror(
                $this->config['drupal_doc_root'] . '/vendor/twbs/bootstrap-sass/assets',
                $this->config['custom_path'] . '/bootstrap/assets'
            );
        } else {
            $this->IO->warning('Failed to copy Bootstrap Sass library.');
        }

        // Copy Bootstrap Sass Drupal styles library.
        $scss = '/web/libraries/drupal-bootstrap-styles/src/3.x.x/8.x-3.x/scss';
        if ($this->fs->exists($this->config['drupal_doc_root'] . $scss)) {
            // Remove to replace if already exist.
            if ($this->fs->exists($this->config['custom_path'] . '/scss')) {
                $this->fs->remove($this->config['custom_path'] . '/scss');
            }
            $this->fs->mirror($this->config['drupal_doc_root'] . $scss, $this->config['custom_path'] . '/scss');
        } else {
            $scss = \str_replace('/web/libraries/', './', $scss);
            $this->IO->warning('Failed to copy Bootstrap Sass Drupal styles files, please follow this step:');
            // @codingStandardsIgnoreLine
            $msg = \sprintf('You must download a copy of Drupal Bootstrap Styles https://github.com/unicorn-fail/drupal-bootstrap-styles and copy over the scss folder located at %s', $scss);
            $this->IO->warning($msg);
        }
    }
}
