<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

use Mog33\Drupal8GenTheme\Generator\BaseGenerator;

/**
 * Generator for drupal/bootstrap_barrio.
 *
 * @link https://www.drupal.org/docs/8/themes/bootstrap-4-sass-barrio-starter-kit
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class BootstrapBarrioSass extends BootstrapBarrio implements PluginGeneratorInterface
{
    /**
     * Settings for this theme generation.
     *
     * @var array
     */
    public static $settings = [
        'id' => 'bootstrap_barrio_sass',
        'base-theme' => 'bootstrap_sass',
        'name_placeholder' => 'bootstrap_sass',
        'title_placeholder' => 'Bootstrap Barrio SubTheme',
        'starterkit_path' => '/themes/contrib/bootstrap_sass',
        'files' => [
            'bootstrap_sass.theme',
            'bootstrap_sass.info.yml',
            'bootstrap_sass.libraries.yml',
            'config/install/bootstrap_sass.settings.yml',
            'config/schema/bootstrap_sass.schema.yml',
            'js/global.js',
        ],
        'dependencies' => [
            'drupal/bootstrap_barrio' => '~4.22',
            'drupal/bootstrap_sass' => '^1.9',
        ],
    ];
}
