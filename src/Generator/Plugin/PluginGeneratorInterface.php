<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

interface PluginGeneratorInterface
{
    public function checkExists(): void;
    public function checkDependencies(): void;
    public function copyFiles(): void;
    public function editFiles(): void;
    public function editYaml(): void;
}
