<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

use Mog33\Drupal8GenTheme\Generator\BaseGenerator;
use Mog33\Drupal8GenTheme\Helpers\Utils;

/**
 * Generator for drupal/bootstrap_barrio.
 *
 * @link https://www.drupal.org/docs/8/themes/barrio-bootstrap-4-drupal-8-theme/creating-a-custom-barrio-sub-theme
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class BootstrapBarrio extends BaseGenerator implements PluginGeneratorInterface
{
    /**
     * Bootstrap version for external source.
     *
     * @var string
     */
    public static $bootstrapVersion = '4.4.1';

    /**
     * Settings for this theme generation.
     *
     * @var array
     */
    public static $settings = [
        'id' => 'bootstrap_barrio',
        'base-theme' => 'bootstrap_barrio',
        'name_placeholder' => 'bootstrap_barrio_subtheme',
        'title_placeholder' => 'Bootstrap Barrio SubTheme',
        'starterkit_path' => '/themes/contrib/bootstrap_barrio/subtheme',
        'files' => [
            '_bootstrap_barrio_subtheme.theme',
            'bootstrap_barrio_subtheme.info.yml',
            'bootstrap_barrio_subtheme.libraries.yml',
            'config/install/bootstrap_barrio_subtheme.settings.yml',
            'config/schema/bootstrap_barrio_subtheme.schema.yml',
            'js/global.js',
            'color/color.inc',
        ],
        'dependencies' => [
            'drupal/bootstrap_barrio' => '~4.22',
            'twbs/bootstrap' => '~4.4',
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function finalize(): void
    {

        // Fix starter kit .theme file name with prefix '_'.
        if ($this->fs->exists($this->config['custom_path'] . '_' . $this->config['name'] . '.theme')) {
            $this->fs->rename(
                $this->config['custom_path'] . '_' . $this->config['name'] . '.theme',
                $this->config['custom_path'] . $this->config['name'] . '.theme'
            );
        }

        // Edit and adapt .libraries.yml file.
        // Update Bootstrap local and cdn. Popper is included in Bootstrap bundle.
        // https://getbootstrap.com/docs/4.4/getting-started/introduction/#js
        $cdn = '//stackpath.bootstrapcdn.com/bootstrap/';
        $edit = [
        'bootstrap' => [
          'js' => [
              '/libraries/bootstrap/dist/js/bootstrap.bundle.min.js' => [],
          ],
          'css' => [
              'component' => [
                  '/libraries/bootstrap/dist/css/bootstrap.min.css' => [],
              ],
          ],
        ],
        'bootstrap_cdn' => [
            'js' => [
                $cdn . self::$bootstrapVersion . '/js/bootstrap.bundle.min.js' => [],
                $cdn . self::$bootstrapVersion . '/css/bootstrap.min.css' => [],
            ],
          ],
        ];
        Utils::yamlEdit($this->config['custom_path'] . $this->config['name'] . '.libraries.yml', $edit);
    }

    /**
     * {@inheritDoc}
     */
    public function setLocal(): void
    {
        // Adapt info.yml if load Bootstrap from local files.
        $add = [
            'libraries' => [
                $this->config['name'] . '/bootstrap',
                $this->config['name'] . '/global-styling',
            ],
        ];
        // Copy Bootstrap library.
        if ($this->fs->exists($this->config['drupal_doc_root'] . '/vendor/twbs/bootstrap/dist')) {
            // Remove to replace if already exist.
            if ($this->config['drupal_doc_root'] . '/web/libraries/bootstrap/dist') {
                $this->fs->remove($this->config['drupal_doc_root'] . '/web/libraries/bootstrap/dist');
            }
            $this->fs->mirror(
                $this->config['drupal_doc_root'] . '/vendor/twbs/bootstrap/dist',
                $this->config['drupal_doc_root'] . '/web/libraries/bootstrap/dist'
            );
        } else {
            $this->IO->warning('Failed to copy Bootstrap library for local use.');
        }
        Utils::yamlEdit($this->config['custom_path'] . $this->config['name'] . '.info.yml', $add);
    }
}
