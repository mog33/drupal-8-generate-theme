<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator\Plugin;

use Mog33\Drupal8GenTheme\Generator\BaseGenerator;

/**
 * Generator for drupal/radix.
 *
 * @link https://drupal-bootstrap.org/api/bootstrap/docs!Sub-Theming.md/group/sub_theming/8.x-3.x
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class Radix extends BaseGenerator implements PluginGeneratorInterface
{
    public static $settings = [
        'id' => 'radix',
        'base-theme' => 'radix',
        'starterkit_path' => '/themes/contrib/radix/src/kits/default',
        'dependencies' => [
            'drupal/radix' => '^4.8',
        ],
    ];
    public function instruction(): void
    {
        $this->IO->write('[WIP] This generator is not yet working.');
    }
    public function checkExists(): void
    {
        $this->IO->notice('Skip check exists.');
    }
    public function checkDependencies(): void
    {
        $this->IO->notice('Skip check dependencies.');
    }
    public function copyFiles(): void
    {
        $this->IO->notice('Skip copy files.');
    }
    public function editFiles(): void
    {
        $this->IO->notice('Skip edit files.');
    }
    public function editYaml(): void
    {
        $this->IO->notice('Skip edit yaml.');
    }
    public function setLocal(): void
    {
        $this->IO->notice('Skip set local.');
    }
    public function finalize(): void
    {
        include_once($this->config['drupal_root'] . '/themes/contrib/radix/src/SubThemeGenerator.php');
        (new \Drupal\radix\SubThemeGenerator())
            ->setDir('web/themes/custom')
            // ->setDir($this->config['drupal_root'] . '/themes/custom')
            ->setMachineName($this->config['name'])
            ->setName($this->config['title'])
            ->generate();
    }
}
