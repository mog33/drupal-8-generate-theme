<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator;

use Composer\Composer;
use Composer\IO\ConsoleIO;
use Symfony\Component\Filesystem\Filesystem;
use Mog33\Drupal8GenTheme\Helpers\Utils;

/**
 * BaseGenerator for all generators.
 *
 * Include common actions to create a subtheme based on a starterkit.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
abstract class BaseGenerator implements BaseGeneratorInterface
{
    /**
     * The Input/Output helper.
     *
     * @var \Composer\IO\ConsoleIO
     */
    protected $io;

    /**
     * The Composer class.
     *
     * @var \Composer\Composer implements GeneratorInterface
     */
    protected $composer;

    /**
     * Generator configuration.
     *
     * @var array
     */
    protected $config;

    /**
     * Generator steps.
     *
     * @var array
     */
    private static $steps = [
        'instruction',
        'checkExists',
        'checkDependencies',
        'copyFiles',
        'editFiles',
        'editYaml',
        'setLocal',
        'finalize',
    ];

    /**
     * BaseGenerator constructor.
     *
     * @param \Composer\IO\ConsoleIO $io
     * @param \Composer\Composer $composer
     * @param array $config
     */
    public function __construct(ConsoleIO $io, Composer $composer, array $config)
    {
        $this->IO = $io;
        $this->composer = $composer;
        $this->config = $config;
        $this->fs = new Filesystem();
    }

    /**
     * Run through the steps to generate the subtheme.
     *
     * @throws \ErrorException if theme creation failed.
     */
    public function generate(): void
    {
        $this->generateSummary();

        // Run methods based on steps.
        foreach (self::$steps as $step) {
            // If we have a config for this step.
            if (isset($this->config[$step])) {
                if ($this->config[$step] && \method_exists($this, $step)) {
                    $this->IO->notice('Step conditional: ' . $step);
                    $this->$step();
                } else {
                    $this->IO->notice('Step skipped: ' . $step);
                }
            } elseif (method_exists($this, $step)) {
                $this->IO->notice('Step: ' . $step);
                $this->$step();
            }
        }
        if ($this->fs->exists($this->config['custom_path'])) {
            $this->IO->write('<info>' . sprintf('Theme created in %s', $this->config['custom_path']) . '</info>');
        } else {
            throw new \ErrorException('Theme creation failed.', 1);
        }
    }

    /**
     * Check if the subtheme already exist.
     *
     * @throws \ErrorException If the starterkit folder is missing or not found.
     */
    public function checkExists(): void
    {
        // Ensure we have a starter kit.
        $this->IO->notice($this->config['drupal_root'] . $this->config['starterkit_path']);
        if (!$this->fs->exists($this->config['drupal_root'] . $this->config['starterkit_path'])) {
            // @codingStandardsIgnoreLine
            throw new \ErrorException(sprintf('Missing starter kit: %s', $this->config['drupal_root'] . $this->config['starterkit_path']), 1);
        }
        // If already generated, ask what to do.
        // @codingStandardsIgnoreLine
        if (!$this->config['force'] && $this->fs->exists($this->config['custom_path'] . $this->config['name'] . '.theme')) {
            $this->IO->warning('Theme already exist in ' . $this->config['custom_path']);
            if ($this->IO->askConfirmation('Delete and replace? This action cannot be undone! (y/n) [n] ', false)) {
                $this->fs->remove($this->config['custom_path']);
            } else {
                $this->IO->warning('SubTheme creation skipped.');
                exit;
            }
        } elseif ($this->config['force'] && $this->fs->exists($this->config['custom_path'])) {
            $this->IO->notice('Force replace existing ' . $this->config['custom_path']);
            $this->fs->remove($this->config['custom_path']);
        }
    }

    /**
     * Check generator dependencies for composer.
     *
     * @throws \ErrorException If a dependency is missing.
     */
    public function checkDependencies(): void
    {
        $dependencies = $this->getDependencies();

        /** @var \Composer\Package\Link */
        $requires = $this->composer->getPackage()->getRequires();

        $installed = array_intersect_key($dependencies, $requires);

        // Check if missing packages.
        if (array_keys($installed) < array_keys($dependencies)) {
            $missing = array_diff_key($dependencies, $installed);

            foreach ($missing as $package => $version) {
                $this->IO->error(sprintf('Missing dependency: %s:%s', $package, $version));
            }
            throw new \ErrorException("Missing packages, please install packages from below.", 1);
        }
        // @todo Check the version match.
      // @todo Option to install/update the packages ?
      /*
      $installedPackages = array_intersect_key($requires, $dependencies);
      $installed = [];
      foreach ($installedPackages as $name => $package) {
        $dependencyVersion = $dependencies[$name];
        $packageVersion = $package->getPrettyConstraint();
        $p = $package->getStability();
        $p = $package->getConstraint();
        \var_export($p);
        $v = $package->getVersion();
        \var_export($v);
        $policy = new DefaultPolicy();
        \var_export($package);
            $installed[$name] = $package->getPrettyConstraint();
        \var_export($installed[$name]);
        \var_export($dependencies[$name]);
      }
      \var_export($installed);
      \var_export($dependencies);
       */
    }

    /**
     * Copy files from the starterkit to the subtheme.
     */
    public function copyFiles(): void
    {
        // Start file creation process.
        $this->fs->mkdir($this->config['custom_path']);
        $this->fs->mirror(
            $this->config['drupal_root'] . $this->config['starterkit_path'],
            $this->config['custom_path']
        );
    }

    /**
     * Perform needed modification to the subtheme files.
     *
     * @throws \ErrorException if a subtheme file is missing.
     */
    public function editFiles(): void
    {
        $files = $this->getFiles();

        if (!$files) {
            $this->IO->notice('No files to copy, skipping.');
        } else {
            // Copy and rename the files.
            foreach ($files as $file) {
                // Prepare new filename.
                $target = str_replace($this->config['name_placeholder'], $this->config['name'], $file);
                $file = $this->config['custom_path'] . $file;

                if ($this->fs->exists($file)) {
                    $content = file_get_contents($file);
                    // Replace machine name in all files.
                    if (!empty($this->config['name_placeholder'])) {
                        $content = str_replace($this->config['name_placeholder'], $this->config['name'], $content);
                    }
                    if (!empty($this->config['title_placeholder'])) {
                        $content = str_replace($this->config['title_placeholder'], $this->config['title'], $content);
                    }
                    $this->fs->remove($file);
                    $this->fs->dumpFile($this->config['custom_path'] . $target, $content);
                } else {
                    $this->fs->remove($this->config['custom_path']);
                    throw new \ErrorException(sprintf('Missing subtheme file: %s', $file), 1);
                }
            }
        }
    }

    /**
     * Edit theme file info.yml.
     */
    public function editYaml(): void
    {
        // The info.yml file need extra edition.
        // Remove the Drupal.org values and replace.
        $remove = [
            'version',
            'core',
            'project',
            'datestamp',
        ];
        $edit = [
            'name' => $this->config['title'],
            'version' => 'VERSION',
            'core' => '8.x',
        ];
        Utils::yamlEdit($this->config['custom_path'] . $this->config['name'] . '.info.yml', $edit, $remove);
    }

    /**
     * Format a summary before generate.
     */
    private function generateSummary(): void
    {
        $summary = \sprintf(
            'Ready to generate theme `%s` [%s] from `%s`.',
            $this->config['title'],
            $this->config['name'],
            $this->config['base-theme'],
        );
        $this->IO->write('<info>' . $summary . '</info>');
    }

    /**
     * Get files from the starterkit to the subtheme.
     *
     * @return array
     */
    private function getFiles(): array
    {
        return $this->config['files'] ?: [];
    }

    /**
     * Get generator project dependencies.
     *
     * @return array
     */
    private function getDependencies(): array
    {
        return $this->config['dependencies'] ?: [];
    }
}
