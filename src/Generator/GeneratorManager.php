<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Generator;

/**
 * GeneratorManager.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class GeneratorManager implements GeneratorManagerInterface
{
    /**
     * Get all available generators with details.
     *
     * @return array
     */
    public static function getGenerators(): array
    {
        $generators = [];
        $path = __DIR__ . '/Plugin/';

        $files = \array_diff(\scandir($path), ['..', '.']);

        foreach ($files as $file) {
            $class = \substr($file, 0, \strrpos($file, "."));
            $className = __NAMESPACE__ . '\\Plugin\\' . \str_replace('-', '', \ucwords($class, '-'));

            if (\is_subclass_of($className, __NAMESPACE__ . '\\' . 'BaseGenerator')) {
                $settings = $className::$settings;
                $generators[$settings['id']] = $settings;
                $generators[$settings['id']]['class'] = $className;
            }
        }
        return $generators;
    }
}
