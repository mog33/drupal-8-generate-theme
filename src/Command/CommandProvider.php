<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Command;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

/**
 * {@inheritdoc}
 */
class CommandProvider implements CommandProviderCapability
{
    /**
     * {@inheritdoc}
     */
    public function getCommands(): array
    {
        return [
            new GenerateCommand(),
            new ListCommand(),
        ];
    }
}
