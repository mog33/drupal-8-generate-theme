<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Generator command.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class GenerateCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
        ->setName('drupal:generate-theme')
        ->setDescription('Generate a subtheme for Drupal 8 based on a theme.')
        ->setDefinition([
            new InputOption('base-theme', 'b', InputOption::VALUE_REQUIRED, 'Base theme used to generate the subtheme'),
            new InputOption('title', '', InputOption::VALUE_REQUIRED, 'Subtheme title'),
            new InputOption('name', '', InputOption::VALUE_REQUIRED, 'Subtheme machine name'),
            new InputOption('local', '', InputOption::VALUE_NONE, 'Load third party libraries from local if available with the base theme'),
            new InputOption('force', 'f', InputOption::VALUE_NONE, 'Do not prompt if subtheme already exist and overwrite'),
        ])
        ->setHelp(
            <<<EOT
            The generate-theme will generate a subtheme on
            your Drupal 8 website from a base theme.
            <info>php composer.phar drupal:generate-theme --base-theme=bootstrap_barrio --title="My theme" --name="my_theme"</info>
            <info>php composer.phar drupal:generate-theme --base-theme=bootstrap_barrio_sass</info>
            EOT
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {

        return (new GenerateTheme($this->getComposer(), $this->getIO()))->execute($input) ? 0 : 1;
    }
}
