<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Command;

use DrupalFinder\DrupalFinder;
use Composer\Composer;
use Composer\IO\IOInterface;
use Symfony\Component\Console\Input\InputInterface;
use Mog33\Drupal8GenTheme\Helpers\Utils;
use Mog33\Drupal8GenTheme\Generator\GeneratorManager;

/**
 * Generate theme plugin.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class GenerateTheme
{
    /**
     * @var \Composer\Composer
     */
    private $composer;

    /**
     * @var \Composer\IO\IOInterface
     */
    private $IO;

    /**
     * Configuration with default values.
     *
     * @var array
     */
    private static $defaultConfig = [
        // 'base-theme' => 'bootstrap_barrio',
        'title' => 'My theme',
        // 'name' => 'my_theme',
        'force' => false,
    ];

    /**
     * Generators manager.
     *
     * @var array
     */
    private $generatorsManager;

    /**
     * @param \Composer\Composer $composer
     * @param \Composer\IO\IOInterface $IO
     */
    public function __construct(Composer $composer, IOInterface $IO)
    {
        $this->composer = $composer;
        /* @var \Composer\IO\BaseIO */
        $this->IO = $IO;
        $this->generatorsManager = GeneratorManager::getGenerators();
    }

    /**
     * Generate execute.
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     *
     * @return int|null
     */
    public function execute(InputInterface $input): ?int
    {
        // Initiate the config.
        $config = $this->buildConfig($input->getOptions());

        $id = $config['base-theme'];
        $this->IO->notice('Instantiate: ' . $id);

        // Dispatch the generator.
        if (isset($this->generatorsManager[$id])) {
            $generator = $this->generatorsManager[$id];
            $config += $generator;
            $this->IO->notice('<info>Generate theme ' . $config['name'] . ' from ' . $id . '</info>');
            $this->IO->notice($config['custom_path']);

            // Instantiate the generator.
            (new $generator['class']($this->IO, $this->composer, $config))->generate();
        } else {
            throw new \InvalidArgumentException(sprintf('No generator found for base theme: %s', $id), 1);
        }
        return 0;
    }

    /**
     * Build configuration based on extra and cli options.
     *
     * @param array $options
     *
     * @throws \ErrorException If Drupal is missing or config failing.
     *
     * @return array
     */
    private function buildConfig(array $options): array
    {
        $config = self::$defaultConfig;
    
        // Add environment configuration options.
        $drupalFinder = new DrupalFinder();

        $config['drupal_doc_root'] = \getcwd();
        $drupalFinder->locateRoot($config['drupal_doc_root']);
        $config['drupal_root'] = $drupalFinder->getDrupalRoot();
        $config['composer_root'] = $drupalFinder->getComposerRoot();

        if (!$config['drupal_root']) {
            throw new \ErrorException('Cannot detect your Drupal instance!', 1);
        }

        // Manage config, set default, merge with extra in composer.json, override
        // with command arguments.
        $configComposer = $this->composer->getPackage()->getExtra();
        if (isset($configComposer['bootstrap-theme'])) {
            $config = \array_merge($config, $configComposer['bootstrap-theme']);
        }

        // Cli command options management.
        $cliOptions = [
            'base-theme' => $options['base-theme'] ?: $config['base-theme'],
            'title' => $options['title'] ?: $config['title'],
            'name' => $options['name'] ?: $config['name'],
            'force' => $options['force'] ?: $config['force'],
            // Conditional config.
            'setLocal' => $options['local'] ?: false,
        ];

        $config = \array_merge($config, $cliOptions);

        // Ensure machine name is valid for the theme.
        $config['name'] = Utils::sanitizeFilename($config['name']);

        $config['custom_path'] = $config['drupal_root'] . '/themes/custom/' . $config['name'] . '/';

        if (!$config['base-theme'] || !$config['name']) {
            throw new \ErrorException('Invalid configuration, please encure you have set a `base-theme` and a `name`.', 1);
        }

        return $config;
    }
}
