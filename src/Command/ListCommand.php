<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Command;

use Composer\Command\BaseCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mog33\Drupal8GenTheme\Generator\GeneratorManager;

/**
 * Generator command.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class ListCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
        ->setName('drupal:generate-theme-list')
        ->setDescription('List all available themes to generate.')
        ->setDefinition([
            new InputOption('online', 'o', InputOption::VALUE_NONE, 'List as one line.'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $IO = $this->getIO();
        $generators = GeneratorManager::getGenerators();

        $shortList = [];
        $fullList = [];
        
        foreach ($generators as $generator) {
            $shortList[] = $generator['id'];
            $fullList[$generator['id']] = [
                $generator['id'],
                $generator['base-theme'],
            ];
            $dependencies = [];
            foreach ($generator['dependencies'] as $dep => $ver) {
                $dependencies[] = $dep . ':' . $ver;
            }
            $fullList[$generator['id']][] = implode(' ', $dependencies);
        }

        $IO->write('<info>List of themes available:</info>');
        if ($input->getOption('online')) {
            $IO->write(\implode(', ', $shortList));
        } else {
            $table = new Table($output);
            $table
                ->setHeaders(['Id', 'Base theme', 'Dependencies'])
                ->setRows($fullList)
            ;
            $table->render();
        }

        return 1;
    }
}
