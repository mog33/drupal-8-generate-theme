<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;
use Mog33\Drupal8GenTheme\Command\CommandProvider;

/**
 * Plugin class.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class Plugin implements PluginInterface, Capable
{
    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getCapabilities(): array
    {
        return [
            CommandProviderCapability::class => CommandProvider::class,
        ];
    }
}
