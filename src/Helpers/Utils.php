<?php

declare(strict_types=1);

namespace Mog33\Drupal8GenTheme\Helpers;

use Symfony\Component\Yaml\Yaml;

/**
 * Helpers and utils class.
 *
 * @author Jean Valverde <contact@developpeur-drupal.com>
 */
class Utils
{
    /**
     * Make a filename safe to use in any function.
     *
     * Clean accents, spaces, special chars...
     * The iconv function must be activated.
     *
     * @param string $fileName
     *   The filename to sanitize (with or without extension).
     * @param string $separator
     *   The default separator.
     * @param bool $lowerCase
     *   Tells if the string must converted to lower case.
     *
     * @author COil <https://github.com/COil>
     * @see http://stackoverflow.com/questions/2668854/sanitizing-strings-to-make-them-url-and-filename-safe
     *
     * @return string
     *   The sanitized full filename.
     */
    public static function sanitizeFilename($fileName, $separator = '_', $lowerCase = true): string
    {
        // If empty stop here.
        if (empty($fileName)) {
            return "";
        }

        // Gather file information and store its extension.
        $fileInfos = \pathinfo($fileName);
        $fileExt = \array_key_exists('extension', $fileInfos) ? '.' . \strtolower($fileInfos['extension']) : '';

        // Removes accents.
        $fileName = @\iconv('UTF-8', 'us-ascii//TRANSLIT', $fileInfos['filename']);

        // Removes all characters that are not separators, letters, numbers,
        // dots or whitespace.
        $fileName = \preg_replace(
            "/[^ a-zA-Z" . \preg_quote($separator) . "\d\.\s]/",
            '',
            $lowerCase ? \strtolower($fileName) : $fileName
        );

        // Replaces all successive separators into a single one.
        $fileName = \preg_replace('![' . \preg_quote($separator) . '\s]+!u', $separator, $fileName);

        // Trim beginning and ending separators.
        $fileName = \trim($fileName, $separator);

        return $fileName . $fileExt;
    }

    /**
     * Load and edit a Yaml file with Symfony Yaml component.
     *
     * @param string $file
     *   The filename to edit, full path with extension.
     * @param array $edit
     *   Map with values for the yaml, will be added or edited if exist.
     * @param array $remove
     *   Map to remove from the Yaml file.
     *
     * @see https://symfony.com/doc/current/components/yaml.html
     */
    public static function yamlEdit($file, array $edit = [], array $remove = []): void
    {
        if ($yaml = Yaml::parseFile($file)) {
            // Remove.
            foreach ($remove as $key) {
                unset($yaml[$key]);
            }
            // Add or edit.
            foreach ($edit as $key => $value) {
                $yaml[$key] = $value;
            }
            $content = Yaml::dump($yaml, 4, 2);
            \file_put_contents($file, $content);
        }
    }
}
