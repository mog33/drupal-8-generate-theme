# Drupal 8 theme generator plugin for Composer

For any issue, please use the [Gitlab-CI issue page](https://gitlab.com/mog33/drupal-8-generate-theme/-/issues).

Composer Plugin for Drupal 8 to create a new theme based on a starterkit.

Currently support drupal/bootstrap and drupal/bootstrap_barrio.

## Install

```
composer require mog33/drupal-8-generate-theme
```

List of available themes and dependencies:

```
composer generate-theme-list
```

You must add the dependencies on your main project, ie for bootstrap:


## Usage

Settings can be done on your composer.json

```json
"extra": {
    "bootstrap-theme": {
        "base-theme": "bootstrap",
        "title": "My BS subtheme",
        "name": "my_subtheme"
    },
```

Then generate the subtheme:

```bash
composer drupal:generate-theme
```

*OR* set options on line command

```bash
composer drupal:generate-theme \
  --base-theme=bootstrap_barrio \
  --title='My BS subtheme' \
  --name='my_subtheme'
```

See help for more detail:

```bash
composer drupal:generate-theme --help
```

## TODO

- Add constraint dependencies check
- Install dependencies?
